class Cell
  attr_accessor :state

  def initialize(state=:dead)
    @state = state
  end

  # Set a new state for the cell
  def step(newstate)
    @state = newstate
  end

  def self.next_state_based_on(current_state, living_neighbours)
    next_state = :dead

    case living_neighbours
    when 2
      next_state = :living if current_state == :living
    when 3
      next_state = :living
    end

    next_state
  end
end
