require 'grid'

class Game
  attr_accessor :iteration, :grid

  def initialize
    @iteration = 0
    @grid = Grid.new
  end

  def start(iterations=200)
    while @iteration < iterations
      grid.print
      grid.step
      puts "-" * @grid.width
      @iteration += 1
    end
  end
end
