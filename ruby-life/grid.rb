require 'cell'

class Grid
  attr_accessor :cells, :width, :height

  def initialize(args=nil)
    @cells = []
    @width = 32
    @height = 32
    unless args.nil?
      @width = args[:width] unless args[:width].nil?
      @height = args[:height] unless args[:height].nil?
    end
    
    (1..@height).each do |y|
      @cells[y-1] = []
      (1..@width).each do |x|
        @cells[y-1][x-1] = Cell.new [:dead, :living][(rand*2).to_i]
      end
    end
  end

  def step
    states = []
    # Calculate next state for all cells in the grid
    (1..@height).each do |y|
      states[y-1] = []
      (1..@width).each do |x|
        current_state = cells[y-1][x-1].state
        states[y-1][x-1] = current_state
        n = neighbours(x-1, y-1)
        n_states = n.map{ |cell| cell.state }
        alive = n_states.reject{ |state| state == :dead }
        living_neighbours = alive.count
        next_state = Cell.next_state_based_on current_state, living_neighbours

        states[y-1][x-1] = next_state
      end
    end
    # Apply the new state to each cell in the grid
    (1..@height).each do |y|
      (1..@width).each do |x|
        @cells[y-1][x-1].state = states[y-1][x-1]
      end
    end
  end

  def print
    puts to_s
  end

  def to_s
    str = ""
    @cells.each do |row|
      str += row.map{ |cell|
        case cell.state
        when :living then '*'
        when :dead then ' '
        end
      }.join("") + "\n"
    end
    str
  end

  private

  def neighbours(x,y)
    neighbours = []

    (y-1..y+1).each do |y1|
      yc = (y1 == @cells.count) ? 0 : y1
      (x-1..x+1).each do |x1|
        xc = (x1 == @cells[yc].count) ? 0 : x1
        neighbours << @cells[yc][xc]
      end
    end

    neighbours
  end
end
